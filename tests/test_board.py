from gomi3.board import *

def test_load():
    b = Board(3)

    state = '''
    ---
    -w-
    b--
    '''

    b.load(state)

    for y in range(3):
        for x in range(3):
            if ((x,y) == (0,2)):
                assert b.color_at(x,y) == Color.BLACK, "(x=0,y=2) should be black"
            elif ((x,y) == (1,1)):
                assert b.color_at(x,y) == Color.WHITE, "(x=1,y=1) should be white"
            else:
                assert b.color_at(x,y) == Color.EMPTY
